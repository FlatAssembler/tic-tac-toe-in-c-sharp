﻿using System;
namespace TicTacToe
{
    public class MainWindow : System.Windows.Forms.Form
    {
        char userSign = 'X';
        char computerSign = 'O';
        bool hasBeenWon = false;
        System.Random random;
        System.Drawing.Graphics graphics;
        System.Drawing.SolidBrush brush;
        System.Drawing.Pen pen;
        System.Windows.Forms.PictureBox pictureBox;
        System.Windows.Forms.Label computerLabel;
        System.Windows.Forms.Label playerLabel;
        System.Windows.Forms.Label tieLabel;
        System.Windows.Forms.Label authorLabel;
        int howManyTimesComputerWon = 0;
        int howManyTimesThePlayerWon=0;
        int howManyTies = 0;
        char[,] table = {
         { ' ', ' ', ' ' },
         { ' ', ' ', ' ' },
         { ' ', ' ', ' ' }
         };
        public MainWindow()
        {
            this.Width = 330;
            this.Height = 400;
            this.Text = "Tic-Tac-Toe in C#";
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            pictureBox = new System.Windows.Forms.PictureBox();
            pictureBox.Left =pictureBox.Top=10;
            pictureBox.Width = 300;
            pictureBox.Height = 300;
            pen = new System.Drawing.Pen(System.Drawing.Color.Black, 2);
            brush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
            computerLabel = new System.Windows.Forms.Label();
            computerLabel.Top = 315;
            computerLabel.Left = 10;
            computerLabel.Width = 100;
            computerLabel.Text = "Computer: 0";
            playerLabel = new System.Windows.Forms.Label();
            playerLabel.Top = 315;
            playerLabel.Left = 110;
            playerLabel.Width = 100;
            playerLabel.Text = "Player: 0";
            tieLabel = new System.Windows.Forms.Label();
            tieLabel.Top = 315;
            tieLabel.Left = 210;
            tieLabel.Width = 100;
            tieLabel.Text = "Tie: 0";
            authorLabel = new System.Windows.Forms.Label();
            authorLabel.Text = "Made by Teo Samarzija, studying at FERIT";
            authorLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            authorLabel.Width = 300;
            authorLabel.Left = 10;
            authorLabel.Top = 340;
            pictureBox.Paint += drawTheTable;
            pictureBox.Click += onClick;
            random = new System.Random();
            this.Controls.Add(pictureBox);
            this.Controls.Add(playerLabel);
            this.Controls.Add(computerLabel);
            this.Controls.Add(tieLabel);
            this.Controls.Add(authorLabel);
        }
        void onClick(object sender, System.EventArgs eventArgs)
        {
            var e = eventArgs as System.Windows.Forms.MouseEventArgs;
            Console.WriteLine("Clicked at coordinates: (" + e.X + "," + e.Y + ")");
            if (hasBeenWon)
            {
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        table[i, j] = ' ';
                hasBeenWon = false;
                if (random.Next() % 2 == 1)
                    letComputerPlay();
                pictureBox.Refresh();
                return;
            }
            int x = e.X / 100;
            int y = e.Y / 100;
            if (table[x, y] == ' ')
            {
                table[x, y] = userSign;
                letComputerPlay();
                pictureBox.Refresh();
            }
        }
        int howManyEmptyFields()
        {
            int empty = 0;
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    if (table[i, j] == ' ') empty++;
            return empty;
        }
        bool tryToFillTheRow(int x)
        {
            if (table[x,0]==' ' && table[x,1]==table[x,2] && table[x,1]!=' ')
            {
                table[x, 0] = computerSign;
                return true;
            }
            if (table[x, 1] == ' ' && table[x, 0] == table[x, 2] && table[x, 0] != ' ')
            {
                table[x, 1] = computerSign;
                return true;
            }
            if (table[x, 2] == ' ' && table[x, 0] == table[x, 1] && table[x, 1] != ' ')
            {
                table[x, 2] = computerSign;
                return true;
            }
            return false;
        }
        bool tryToFillTheColumn(int y)
        {
            if (table[0,y]==' ' && table[1,y]==table[2,y] && table[1,y]!=' ')
            {
                table[0, y] = computerSign;
                return true;
            }
            if (table[1,y] == ' ' && table[0,y] == table[2,y] && table[0,y] != ' ')
            {
                table[1,y] = computerSign;
                return true;
            }
            if (table[2,y] == ' ' && table[0,y] == table[1,y] && table[1,y] != ' ')
            {
                table[2,y] = computerSign;
                return true;
            }
            return false;
        }
        int countSignsAround(int x,int y, char sign)
        {
            int count = 0;
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    if (x + i < 0 || x + i > 2 || y + j < 0 || y + j > 2) continue;
                    if (i == 0 && j == 0) continue;
                    if (table[x + i, y + j] == sign) count++;
                }
            return count;
        }
        void letComputerPlay()
        {
            int empty = howManyEmptyFields();
            if (empty == 0) return;
            if (table[1, 1] == ' ') //Ako je centar prazan, stavi svoj znak u centar, da igrac ne moze popuniti dijagonale.
            {
                table[1, 1] = computerSign;
                return;
            }
            if (tryToFillTheRow(0)) return;
            if (tryToFillTheRow(1)) return;
            if (tryToFillTheRow(2)) return;
            if (tryToFillTheColumn(0)) return;
            if (tryToFillTheColumn(1)) return;
            if (tryToFillTheColumn(2)) return;
            if (table[0,0]==' ' && table[1,1]==table[2,2] && table[1,1]!=' ') //Pokusaj sprijeciti igraca da popuni prvu dijagonalu od gore.
            {
                table[0, 0] = computerSign;
                return;
            }
            if (table[0,2]==' ' && table[1,1]==table[2,0] && table[1,1]!=' ') //Pokusaj sprijeciti igraca da popuni drugu dijagonalu od dolje.
            {
                table[0, 2] = computerSign;
                return;
            }
            if (table[2,0]==' ' && table[1,1]==table[0,2] && table[1,1]!=' ') //Pokusaj sprijeciti igraca da popuni drugu dijagonalu od dolje.
            {
                table[2, 0] = computerSign;
                return;
            }
            if (table[2,2]==' ' && table[1,1]==table[0,0] && table[1,1]!=' ') //Pokusaj sprijeciti igraca da popuni prvu dijagonalu od dolje.
            {
                table[2, 2] = computerSign;
                return;
            }
            for (int x=0; x<2; x++) //Pokusaj napraviti "dvostrukost", da, ako te igrac pokusa sprijeciti u jednom putu do pobijede, jos uvijek imas drugi.
                for (int y=0; y<2; y++)
                    if (table[x,y]==' ' && countSignsAround(x,y,computerSign)>1)
                    {
                        table[x, y] = computerSign;
                        return;
                    }
            for (int x = 0; x < 2; x++)
                for (int y = 0; y < 2; y++)
                    if (table[x, y] == ' ' && countSignsAround(x, y, userSign) > 1) //Pokusaj sprijeciti igraca da napravi "dvostrukost".
                    {
                        table[x, y] = computerSign;
                        return;
                    }
            //Ako ne mozes popuniti niti jedan red, niti jedan stupac, a niti jednu dijagonalu, te za sada ne mozes napraviti "dvostrukost", igraj nasumicno.
            int i, j;
            do 
            {
                i = random.Next() % 3;
                j = random.Next() % 3;
            }
            while (table[i, j] != ' ');
            table[i, j] = computerSign;

        }
        void drawTheTable(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Console.WriteLine("Drawing the table:");
            Console.WriteLine("+-+-+-+");
            for (int i=0; i<3; i++)
            {
                Console.Write("|");
                for (int j = 0; j < 3; j++)
                    Console.Write(table[i, j].ToString()+"|");
                Console.WriteLine("");
                Console.WriteLine("+-+-+-+");
            }
            pen.Color = System.Drawing.Color.Black;
            graphics = e.Graphics;
            graphics.FillRectangle(brush, 0, 0, 300, 300);
            graphics.DrawLine(pen, 100,0,100,300);
            graphics.DrawLine(pen, 200, 0,200, 300);
            graphics.DrawLine(pen, 0, 100, 300,100);
            graphics.DrawLine(pen, 0, 200, 300, 200);
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    if (table[i,j] == 'X')
                    {
                        graphics.DrawLine(pen, i * 100 + 10, j * 100 + 10, i * 100 + 90, j * 100 + 90);
                        graphics.DrawLine(pen, i * 100 + 90, j * 100 + 10, i * 100 + 10, j * 100 + 90);
                    }
                    else if (table[i,j]=='O')
                    {
                        graphics.DrawEllipse(pen, i * 100 + 10, j * 100 + 10, 80, 80);
                    }
                }
            checkIfGameIsWon();
        }
        bool areThreeEqual(char a, char b, char c)
        {
            return a == b && b == c;
        }
        void checkIfGameIsWon()
        {
            pen.Color = System.Drawing.Color.Red;
            char whoHasWon = ' ';
            if (table[0, 0] !=' ' && areThreeEqual(table[0,0],table[1,1],table[2,2])) //Prva dijagonala
            {
                graphics.DrawLine(pen, 50, 50, 250, 250);
                whoHasWon = table[0, 0];
            }
            else if (table[0,2]!=' ' && areThreeEqual(table[0,2],table[1,1],table[2,0])) //Druga dijagonala
            {
                graphics.DrawLine(pen, 50, 250, 250, 50);
                whoHasWon = table[0, 2];
            }
            else if (table[0,0]!=' ' && areThreeEqual(table[0,0],table[0,1],table[0,2])) //Prvi redak
            {
                graphics.DrawLine(pen, 50, 50, 50, 250);
                whoHasWon = table[0, 0];
            }
            else if (table[1,0]!=' ' && areThreeEqual(table[1,0],table[1,1],table[1,2])) //Drugi redak
            {
                graphics.DrawLine(pen, 150, 50, 150, 250);
                whoHasWon = table[1, 0];
            }
            else if (table[2,0]!=' ' && areThreeEqual(table[2,0],table[2,1],table[2,2])) //Treci redak
            {
                graphics.DrawLine(pen, 250, 50, 250, 250);
                whoHasWon = table[2, 0];
            }
            else if (table[0,0]!=' ' && areThreeEqual(table[0,0],table[1,0],table[2,0])) //Prvi stupac
            {
                graphics.DrawLine(pen, 50, 50, 250, 50);
                whoHasWon = table[0, 0];
            }
            else if (table[0,1]!=' ' && areThreeEqual(table[0,1],table[1,1],table[2,1])) //Drugi stupac
            {
                graphics.DrawLine(pen, 50, 150, 250, 150);
                whoHasWon = table[0, 1];
            }
            else if (table[0,2]!=' ' && areThreeEqual(table[0,2],table[1,2],table[2,2])) //Treci stupac
            {
                graphics.DrawLine(pen, 50, 250, 250, 250);
                whoHasWon = table[0, 2];
            }
            if (whoHasWon != ' ' && !hasBeenWon)
            {
                hasBeenWon = true;
                if (whoHasWon == computerSign) howManyTimesComputerWon++;
                else howManyTimesThePlayerWon++;
                char tmp = computerSign;
                computerSign = userSign;
                userSign = tmp;
                computerLabel.Text = "Computer: " + howManyTimesComputerWon;
                playerLabel.Text = "Player: " + howManyTimesThePlayerWon;
                System.Windows.Forms.MessageBox.Show("'" + whoHasWon + "' has won!");
            } else if (!hasBeenWon && howManyEmptyFields()==0)
            {
                hasBeenWon = true;
                howManyTies++;
                tieLabel.Text = "Tie: " + howManyTies;
                System.Windows.Forms.MessageBox.Show("Tie!");
            }
        }
        public static void Main()
        { 
            System.Windows.Forms.Application.Run(new MainWindow());
        }
    }
}
